#include <BluePlanet.h>

BMPImage::BMPImage(void) {
	height = 0;
	lpBMP = NULL;
	lpBuf = NULL;
	lpInfo = NULL;
	width = 0;
}

BMPImage::~BMPImage(void) {
	GlobalFree(lpBuf);
}

BMPImage::BMPImage(int width, int height) {
	this->lpBuf = NULL;
	this->lpBMP = NULL;
	this->lpInfo = NULL;
	this->width = width;
	this->height = height;
}

BMPImage::BMPImage(RString string) {
	File* file = new File(string);
	int size = file->getFileSize();
	lpBuf = (LPBYTE) GlobalAlloc(GPTR, size);

	file->readByte(lpBuf, 0, size);
	lpInfo = (LPBITMAPINFO) (lpBuf + sizeof(BITMAPFILEHEADER));
	lpBMP = lpBuf + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	width = lpInfo->bmiHeader.biWidth;
	height = lpInfo->bmiHeader.biHeight;

	delete file;
}

LPBITMAPINFO BMPImage::getBitmapInfo(void) {
	return lpInfo;
}

LPBYTE BMPImage::getBytes(void) {
	return lpBMP;
}

int BMPImage::getWidth(void) {
	return width;
}

int BMPImage::getHeight(void) {
	return height;
}

void BMPImage::release(void) {
	GlobalFree(lpBuf);
}
