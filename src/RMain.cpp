#include <RMain.h>
#include <WoolMarkCanvas.h>
#include <BluePlanet.h>

int WINAPI WinMain(HINSTANCE hThisInst, HINSTANCE hPrevInst, LPSTR lpszArgs,
		int nWinMode) {
	char* className = { (char*) "window" };
	char* title = { (char*) "Woolmark" };
	CAppInitializer::init(hThisInst, hPrevInst, lpszArgs, nWinMode);
	CWindowApp App;
	Display* display = Display::getDisplay();
	WoolMarkCanvas canvas;
	display->setCurrent(&canvas);

	App.run(className, title, 120, 120, NULL, false);

	return 1;
}
