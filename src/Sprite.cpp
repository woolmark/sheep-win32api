#include <BluePlanet.h>

Sprite::Sprite(void) {
	columns = 0;
	rows = 0;
	tileWidth = 0;
	tileHeight = 0;
	image = NULL;
	currentCell = 0;
}

Sprite::Sprite(Image* srcImage, int tWidth, int tHeight) {
	image = srcImage;
	tileWidth = tWidth;
	tileHeight = tHeight;
	columns = image->getWidth() / tileWidth;
	rows = image->getHeight() / tileHeight;
	currentCell = 0;
}

Sprite::~Sprite(void) {
}

void Sprite::setIndex(int index) {
	currentCell = index;
}

int Sprite::getTiledWidth(void) {
	return tileWidth;
}

int Sprite::getTiledHeight(void) {
	return tileHeight;
}

int Sprite::getCellX(void) {
	for (int i = 0; i < rows; i++) {
		if (currentCell < (i + 1) * columns) {
			int temp = currentCell - (i * columns);
			return temp * tileWidth;
		}
	}

	return -1;
}

int Sprite::getCellY(void) {
	for (int i = 0; i < rows; i++) {
		if (currentCell < (i + 1) * columns) {
			return i * tileHeight;
		}
	}
	return -1;
}

Image* Sprite::getImage(void) {
	return image;
}
