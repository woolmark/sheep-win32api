#include <BluePlanet.h>

RString::RString(void) {
	s = new char[1];
	*s = 0;

	buffer = new char[1];
	*buffer = 0;
	bufferFlag = false;
}

RString::RString(const char* str) {
	s = new char[strlen(str) + 1];
	strcpy(s, str);

	buffer = new char[1];
	*buffer = 0;
	bufferFlag = false;
}

RString::RString(const char* str, int offset, int length) {
	s = new char[length + 1];

	for (int i = 0; i < length; i++) {
		s[i] = str[offset + i];
	}

	s[length] = '\0';

	buffer = new char[1];
	*buffer = 0;
	bufferFlag = false;
}

RString::RString(const RString& str) {
	s = new char[strlen(str.s) + 1];
	strcpy(s, str.s);
	buffer = new char[1];
	*buffer = 0;
	bufferFlag = false;
}

RString::~RString() {
	delete[] s;
	delete[] buffer;
}

char RString::charAt(int index) {
	return s[index];
}

bool RString::equals(const char* str) {
	if (strlen(s) != strlen(str))
		return false;
	for (int i = 0; i < (int) strlen(s); i++) {
		if (s[i] != str[i])
			return false;
	}
	return true;
}

char* RString::getChar(void) {
	return s;
}

int RString::length(void) {
	return strlen(s);
}

RString RString::subString(int biginIndex, int endIndex) {
	if ((int) strlen(s) < endIndex) {
		return RString(s);
	}

	delete[] buffer;
	buffer = new char[endIndex - biginIndex + 1];
	memmove(buffer, s + biginIndex, endIndex - biginIndex);
	buffer[endIndex - biginIndex] = '\0';

	return RString(buffer);
}

RString RString::valueOf(int integer) {
	char buf[33];
	wsprintf(buf, "%d", integer);
	RString str(buf);

	return str;
}

RString RString::valueOf(long longValue) {
	char buf[33];
	wsprintf(buf, "%ld", longValue);
	RString str(buf);

	return str;
}

RString::operator char*(void) {
	delete[] buffer;
	buffer = new char[strlen(s) + 1];
	strcpy(buffer, s);
	return buffer;
}

RString RString::operator=(const char* str) {
	delete[] s;
	s = new char[strlen(str) + 1];
	strcpy(s, str);

	return (*this);
}

RString RString::operator=(const RString str) {
	delete[] s;
	s = new char[strlen(str.s) + 1];
	strcpy(s, str.s);

	return (*this);
}

RString operator+(const RString& str1, const RString& str2) {
	int length = strlen(str1.s) + strlen(str2.s);
	char* p = new char[length + 1];
	strcpy(p, str1.s);
	strcat(p, str2.s);
	RString buffer(p);
	delete[] p;
	return buffer;
}

