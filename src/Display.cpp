#include <BluePlanet.h>

int Display::width = 0;
int Display::height = 0;
Display* Display::instance = NULL;

const int Display::LEFT_MOUSE_DOUBLECLICK = WM_LBUTTONDBLCLK;
const int Display::LEFT_MOUSE_DOWN = WM_LBUTTONDOWN;
const int Display::LEFT_MOUSE_UP = WM_LBUTTONUP;
const int Display::RIGHT_MOUSE_DOUBLECLICK = WM_RBUTTONDBLCLK;
const int Display::RIGHT_MOUSE_DOWN = WM_RBUTTONDOWN;
const int Display::RIGHT_MOUSE_UP = WM_RBUTTONUP;
const int Display::KEYBOARD_DOWN = WM_KEYDOWN;
const int Display::KEYBOARD_UP = WM_KEYUP;

Display::Display(void) {
	canvas = NULL;;
	g = NULL;
}

Display* Display::getDisplay(void) {
	if (instance == NULL) {
		instance = new Display();
	}
	return instance;
}

int Display::getHeight(void) {
	return height;
}

int Display::getWidth(void) {
	return width;
}

void Display::releaseDisplay(void) {
	delete instance;
}

void Display::keyBoardEvent(int event, int vKey) {
	canvas->keyBoardEvent(event, vKey);
}

void Display::mouseEvent(int button) {
	canvas->mouseEvent(button);
}

void Display::setCurrent(Canvas* screen) {
	canvas = screen;
}

void Display::setWindowSize(int x, int y) {
	width = x;
	height = y;
}

void Display::initCanvas(void) {
	canvas->init();
}

void Display::startPaint(Graphics* g) {
	canvas->paint(g);
}
