#include <BluePlanet.h>
#include <WoolMarkCanvas.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

static BOOL isClosed = false;

void WoolMarkCanvas::calc() {
	if (flag) {
		addSheep();
	}

	for (int i = 0; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] >= 0) {
			sheepPos[i][0] -= 5;
			if ((sheepPos[i][3] - 20) < sheepPos[i][0]
					&& sheepPos[i][0] <= sheepPos[i][3]) {
				sheepPos[i][1] -= 3;
			}

			else {
				if ((sheepPos[i][3] - 40) < sheepPos[i][0]
						&& sheepPos[i][0] <= (sheepPos[i][3] - 20)) {
					sheepPos[i][1] += 3;
				}
				if (sheepPos[i][0] < (sheepPos[i][3] - 10)
						&& sheepPos[i][2] == 0) {
					sheepNumber++;
					sheepPos[i][2] = 1;
				}
			}

			if (sheepPos[i][0] < 0) {
				if (i == 0) {
					setSheepInitPosition(sheepPos[0]);
				} else {
					clearSheepPosition(sheepPos[i]);
				}
			}
		}
	}
	sheepState++;
}

void* threadMain(void* p) {
	WoolMarkCanvas* canvas = (WoolMarkCanvas*) p;
	timespec spec;
	spec.tv_sec = 0;
	spec.tv_nsec = 1000 * 1000 * 60;
	while (!isClosed) {
		canvas->calc();
		canvas->repaint();
		nanosleep(&spec, NULL);
	}
	return NULL;
}

void WoolMarkCanvas::clearSheepPosition(int* pos) {

	pos[0] = 0;
	pos[1] = -1;
	pos[2] = 0;
	pos[3] = 0;
}

int WoolMarkCanvas::getJumpX(int y) {
	y -= (HEIGHT - 40);
	return (70 - 3 * y / 4);
}

void WoolMarkCanvas::setSheepInitPosition(int* pos) {
	pos[0] = WIDTH - 20;
	pos[1] = HEIGHT - 35 + (rand() % 20);
	pos[2] = 0;
	pos[3] = getJumpX(pos[1]);
}

void WoolMarkCanvas::init(void) {

	pthread_t thread;
	setMouseListener(this);
	sheepNumber = 1;

	sheepImg = Image::createImage("res/sheep.bmp");
	backgroundImg = Image::createImage("res/back.bmp");
	sheep = new Sprite(sheepImg, 20, 12);
	sheep->setIndex(1);

	for (int i = 0; i < SHEEP_MAX; i++) {
		clearSheepPosition(sheepPos[i]);
	}

	setSheepInitPosition(sheepPos[0]);
	pthread_create(&thread, NULL, threadMain, (void *) this);
	flag = false;
	sheepState = 0;
}

WoolMarkCanvas::~WoolMarkCanvas(void) {
	isClosed = true;
	sheepImg->release();
	delete backgroundImg;
	delete sheep;
}

void WoolMarkCanvas::paint(Graphics* g) {
	g->setColor(GROUND_COLOR);
	g->fillRect(0, 0, WIDTH, HEIGHT);
	g->setColor(SKY_COLOR);
	g->fillRect(0, 0, WIDTH, HEIGHT - 70);
	g->drawImage(backgroundImg, 35, HEIGHT - 80);

	for (int i = 0; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] >= 0) {
			sheep->setIndex(sheepState%2);
			g->drawSprite(sheep, sheepPos[i][0], sheepPos[i][1]);
		}
	}

	g->setColor(TEXT_COLOR);
	char text[128];
	sprintf(text, "�r�� %d �C", sheepNumber);

	g->drawString(text, 5, 15);
}

void WoolMarkCanvas::mouseEvent(int button) {
	if (button == MouseListener::MOUSE_LEFT_DOWN) {
		flag = true;
	} else {
		flag = false;
	}
}

void WoolMarkCanvas::addSheep() {
	for (int i = 1; i < SHEEP_MAX; i++) {
		if (sheepPos[i][1] == -1) {
			setSheepInitPosition(sheepPos[i]);
			return;
		}
	}
}

