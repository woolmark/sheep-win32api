#include <BluePlanet.h>

const int ImageButton::TYPE_IMAGE = 0;
const int ImageButton::TYPE_SPRITE = 1;

ImageButton::ImageButton(void) {
	height = 0;
	width = 0;
	x = 0;
	y = 0;
	type = 0;
	img = NULL;
	sp = NULL;
	buttonAlive = true;
}

ImageButton::ImageButton(Image* image, int ix, int iy) {
	sp = NULL;
	type = ImageButton::TYPE_IMAGE;
	img = image;
	width = image->getWidth();
	height = image->getHeight();
	x = ix;
	y = iy;
	buttonAlive = true;
}

ImageButton::ImageButton(Sprite* sprite, int ix, int iy) {
	img = NULL;
	type = ImageButton::TYPE_SPRITE;
	sp = sprite;
	width = sprite->getTiledWidth();
	height = sprite->getTiledHeight();
	x = ix;
	y = iy;
	buttonAlive = true;
}

Image* ImageButton::getImage(void) {
	return img;
}

Sprite* ImageButton::getSprite(void) {
	return sp;
}

int ImageButton::getType(void) {
	return type;
}

int ImageButton::getX(void) {
	return x;
}

int ImageButton::getY(void) {
	return y;
}

bool ImageButton::isMouseOver(void) {
	int mx = MousePosision::getMouseX();
	int my = MousePosision::getMouseY();

	if (x > mx || x + width < mx) {
		return false;
	}
	if (y > my || y + height < my) {
		return false;
	}

	return true;
}

bool ImageButton::isAlive(void) {
	return buttonAlive;
}

void ImageButton::setAlive(bool flag) {
	buttonAlive = flag;
}
