#include <BluePlanet.h>

CWindowApp::CWindowApp(void) {
	windowClass = new WindowClass();
	window = new Window();
	msg = new MSG();
}

CWindowApp::~CWindowApp(void) {
	delete windowClass;
	delete window;
	delete msg;
}

void CWindowApp::run(char* className, char* title, int x, int y, WORD ICON,
		bool fullscreen) {
	windowClass->setWindowClassName(className);
	windowClass->setIcon(ICON);
	windowClass->setSmIcon(ICON);
	windowClass->setWindowProc();
	windowClass->registerWindowClass();

	window->createWindow(className, title, x, y, fullscreen);

	while (GetMessage(msg, NULL, 0, 0)) {
		TranslateMessage(msg);
		DispatchMessage(msg);
	}
}
