#include <BluePlanet.h>

Canvas::Canvas(void) {
	mouseListener = NULL;
	keyboardListener = NULL;
}

void Canvas::keyBoardEvent(int event, int vKey) {
	if (keyboardListener != NULL)
		keyboardListener->keyBoardEvent(event, vKey);
}

void Canvas::mouseEvent(int button) {
	if (mouseListener != NULL)
		mouseListener->mouseEvent(button);
}

void Canvas::repaint(void) {
	InvalidateRect(CAppInitializer::getHWND(), NULL, FALSE);
}

void Canvas::setKeyboardListener(KeyboardListener* listener) {
	keyboardListener = listener;
}

void Canvas::setMouseListener(MouseListener* listener) {
	mouseListener = listener;
}

