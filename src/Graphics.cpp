#include <BluePlanet.h>

Graphics* Graphics::instance = NULL;

Graphics::Graphics(void) {
	brushColor = 0;
	penColor = 0;

	HBITMAP tmpBMP;
	HDC tmpHdc;
	HWND tmphWnd = CAppInitializer::getHWND();
	lpInfo = new BITMAPINFO();
	lpInfo->bmiHeader.biBitCount = 32;
	lpInfo->bmiHeader.biClrImportant = 0;
	lpInfo->bmiHeader.biClrUsed = 0;
	lpInfo->bmiHeader.biCompression = BI_RGB;
	lpInfo->bmiHeader.biHeight = 480;
	lpInfo->bmiHeader.biPlanes = 1;
	lpInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	lpInfo->bmiHeader.biWidth = 640;
	lpInfo->bmiHeader.biXPelsPerMeter = 0;
	lpInfo->bmiHeader.biYPelsPerMeter = 0;

	tmpHdc = GetDC(tmphWnd);
	tmpBMP = CreateDIBSection(tmpHdc, lpInfo, DIB_RGB_COLORS,
			(LPVOID *) (&lpBuf), NULL, 0);
	hdc = CreateCompatibleDC(tmpHdc);

	SelectObject(hdc, tmpBMP);
	ReleaseDC(tmphWnd, tmpHdc);
	DeleteObject(tmpBMP);
}

Graphics::~Graphics(void) {
	DeleteObject(hdc);
	delete lpInfo;
}

void Graphics::disposeGraphics(void) {
	delete instance;
}

HDC CreateEmptyBMP(HDC hdc, int width, int height) {
	HBITMAP hbmp;
	HDC hdc_work;

	hbmp = CreateCompatibleBitmap(hdc, width, height);
	hdc_work = CreateCompatibleDC(hdc);
	SelectObject(hdc_work, hbmp);
	PatBlt(hdc_work, 0, 0, width, height, WHITENESS); // 白で塗りつぶす
	DeleteObject(hbmp);
	return hdc_work;
}

void Graphics::drawImage(Image* img, int x, int y) {
	HDC tmpHdc = CreateEmptyBMP(hdc,img->getWidth() ,img->getHeight());
	StretchDIBits(tmpHdc, 0, 0, img->getWidth(), img->getHeight(), 0, 0,
			img->getWidth(), img->getHeight(), img->getBytes(),
			img->getBitmapInfo(),
			DIB_RGB_COLORS, SRCCOPY);

	TransparentBlt(hdc, x, y, img->getWidth(), img->getHeight(), tmpHdc,
			0, 0, img->getWidth(), img->getHeight(),RGB(0x22, 0xb1, 0x4c));
}

void Graphics::drawImageButton(ImageButton* imagebutton) {
	if (!imagebutton->isAlive())
		return;
	if (imagebutton->getType() == ImageButton::TYPE_IMAGE) {
		Image* img = imagebutton->getImage();
		StretchDIBits(hdc, imagebutton->getX(), imagebutton->getY(),
				img->getWidth(), img->getHeight(), 0, 0, img->getWidth(),
				img->getHeight(), img->getBytes(), img->getBitmapInfo(),
				DIB_RGB_COLORS, SRCCOPY);
	} else if (imagebutton->getType() == ImageButton::TYPE_SPRITE) {
		Sprite* sprite = imagebutton->getSprite();
		int spriteY = sprite->getImage()->getHeight() - sprite->getCellY()
				- sprite->getTiledHeight();
		StretchDIBits(hdc, imagebutton->getX(), imagebutton->getY(),
				sprite->getTiledWidth(), sprite->getTiledHeight(),
				sprite->getCellX(), spriteY, sprite->getTiledWidth(),
				sprite->getTiledHeight(), sprite->getImage()->getBytes(),
				sprite->getImage()->getBitmapInfo(), DIB_RGB_COLORS, SRCCOPY);
	} else {
	}
}

void Graphics::drawRect(int x, int y, int width, int height) {
	HPEN hPen = CreatePen(PS_SOLID, 1, penColor);
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, x, y, x + width, y + height);
	DeleteObject(hPen);
}

void Graphics::drawSprite(Sprite* sprite, int x, int y) {
	HDC tmpHdc = CreateEmptyBMP(hdc,sprite->getTiledWidth() ,sprite->getTiledHeight());
	StretchDIBits(tmpHdc, 0, 0, sprite->getTiledWidth(), sprite->getTiledHeight(), 0, 0,
			sprite->getTiledWidth(), sprite->getTiledHeight(), sprite->getImage()->getBytes(),
			sprite->getImage()->getBitmapInfo(),
			DIB_RGB_COLORS, SRCCOPY);

	TransparentBlt(hdc, x, y, sprite->getTiledWidth(), sprite->getTiledHeight(), tmpHdc,
			0, 0, sprite->getTiledWidth(), sprite->getTiledHeight(),RGB(0x22, 0xb1, 0x4c));
}

void Graphics::drawString(char* string, int x, int y) {
	HFONT hFont;
	SetBkMode(hdc, TRANSPARENT);
	hFont = CreateFont(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET,
	OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
	DEFAULT_QUALITY, DEFAULT_PITCH, "MS Pゴシック");
	SelectObject(hdc, hFont);
	TextOut(hdc, x, y, string, strlen(string));
	DeleteObject(hFont);
}

void Graphics::fillRect(int x, int y, int width, int height) {
	HPEN hPen = CreatePen(PS_SOLID, 1, penColor);
	HBRUSH hBrush = CreateSolidBrush(brushColor);
	SelectObject(hdc, hPen);
	SelectObject(hdc, hBrush);
	Rectangle(hdc, x, y, x + width, y + height);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

HDC Graphics::getHDC(void) {
	return hdc;
}

Graphics* Graphics::getInstance(void) {
	if (instance == NULL) {
		instance = new Graphics();
	}
	return instance;
}

void Graphics::setColor(int color) {
	WORD r, g, b;
	r = color >> 16;
	g = (color & 0x0000ff00) >> 8;
	b = color & 0x000000ff;

	brushColor = RGB(r, g, b);
	penColor = RGB(r, g, b);
}
