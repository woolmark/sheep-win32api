#pragma once

#include <Window.h>
#include <WindowClass.h>

class CWindowApp
{
private:
  MSG* msg;
  WindowClass* windowClass;
  Window* window;
public:
  CWindowApp(void);
  ~CWindowApp(void);
  void run(char* className, char* title, int x, int y, WORD ICON, bool fullscreen);
};
