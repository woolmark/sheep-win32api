#pragma once

class KeyboardListener {
public:
	virtual ~KeyboardListener() {
	}
	virtual void keyBoardEvent(int event, int vKey) = 0;
};
