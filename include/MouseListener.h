#pragma once

class MouseListener {
public:
	static const int MOUSE_LEFT_DOWN = 513;
	static const int MOUSE_LEFT_UP = 514;
	virtual ~MouseListener() {
	}
	virtual void mouseEvent(int button) = 0;
};
