#pragma once

class RString {
private:
	char* s;
	char* buffer;
	bool bufferFlag;
public:
	RString(void);
	RString(const char* str);
	RString(const char* str, int offset, int length);
	RString(const RString& str);
	~RString(void);
	char charAt(int index);
	bool equals(const char* str);
	char* getChar(void);
	int length(void);
	RString subString(int beginIndex, int endIndex);
	static RString valueOf(int integer);
	static RString valueOf(long longValue);
	operator char*();
	RString operator=(const char* str);
	RString operator=(const RString str);
	friend RString operator+(const RString& str1, const RString& str2);
};
